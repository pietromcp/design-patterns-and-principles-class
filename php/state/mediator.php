<?php

interface SignallingGateway {
    public function sendVideoOffer($offer);

    public function setMediator(Mediator $mediator);
}

interface Connection {
    public function receiveVideoAnswer($answer);

    public function setMediator(Mediator $mediator);
}

interface Mediator {
    public function handleVideoOffer($offer);

    public function handleVideoAnswer($answer);
}

class VideoCallCoordinator implements Mediator {
    private Connection $connection;
    private SignallingGateway $signaller;

    function __construct(Connection $connection, SignallingGateway $signaller) {
        $this->connection = $connection;
        $this->signaller = $signaller;
        $this->connection->setMediator($this);
        $this->signaller->setMediator($this);
    }

    public function handleVideoOffer($offer) {
        $this->signaller->sendVideoOffer($offer);
    }

    public function handleVideoAnswer($answer) {
        $this->connection->receiveVideoAnswer($answer);
    }
}

class FakeConnection implements Connection {
    private Mediator $mediator;

    public function setMediator($mediator) {
        $this->mediator = $mediator;
    }

    public function receiveVideoAnswer($answer) {
        echo "Video answer received: $answer\n";
    }

    public function triggerVideoOfferReady($offer) {
        $this->mediator->handleVideoOffer($offer);
    }
}

class FakeSignaller implements SignallingGateway {
    private Mediator $mediator;

    public function setMediator($mediator) {
        $this->mediator = $mediator;
    }

    public function sendVideoOffer($offer) {
        echo "Sending video offer: $offer\n";
    }

    public function triggerVideoAnswerReceived($answer) {
        $this->mediator->handleVideoAnswer($answer);
    }
}

$signaller = new FakeSignaller();
$connection = new FakeConnection();
$coordinator = new VideoCallCoordinator($connection, $signaller);

$connection->triggerVideoOfferReady("Video Ofer 123");
$signaller->triggerVideoAnswerReceived("Video Answer 456");