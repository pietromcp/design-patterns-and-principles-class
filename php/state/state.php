<?php

interface TrafficLightState {
    public function clock(): TrafficLightState;

    public function pedestrianCall();

}

class Red implements TrafficLightState {
    private int $changeIn = 8;

    public function clock(): TrafficLightState {
        $this->changeIn--;
        return ($this->changeIn == 0) ? new Green() : $this;
    }

    public function pedestrianCall() {
    }
}

class Yellow implements TrafficLightState {
    private int $changeIn = 3;

    public function clock(): TrafficLightState {
        $this->changeIn--;
        return ($this->changeIn == 0) ? new Red() : $this;
    }

    public function pedestrianCall() {
    }
}

class Green implements TrafficLightState {
    private int $changeIn = 7;

    public function clock(): TrafficLightState {
        $this->changeIn--;
        return ($this->changeIn == 0) ? new Yellow() : $this;
    }

    public function pedestrianCall() {
        if ($this->changeIn > 2) {
            $this->changeIn = 2;
        }
    }
}

class TrafficLight {
    private TrafficLightState $state;

    public function __construct(TrafficLightState $state) {
        $this->state = $state;
    }

    public function clockElapsed() {
        $this->state = $this->state->clock();
    }

    public function receivePedestrianCall() {
        $this->state->pedestrianCall();
    }

    public function showState(): string {
        return strtoupper(get_class($this->state));
    }
}

function printState(TrafficLight $light) {
    echo 'State is ' . $light->showState() . "\n";
}

function execAndPrint($light, $doSomething) {
    $doSomething($light);
    printState($light);
}


$clock = function(TrafficLight $l) { $l->clockElapsed(); };
$call = function(TrafficLight $l) { $l->receivePedestrianCall(); };

$light = new TrafficLight(new Yellow());

echo 'First TrafficLight....';
printState($light);
for ($i = 0; $i < 18; $i++) {
    execAndPrint($light, $clock);
}
echo 'Second TrafficLight....';
$light = new TrafficLight(new Green());
printState($light);
execAndPrint($light, $clock);
execAndPrint($light, $call);
execAndPrint($light, $clock);
execAndPrint($light, $clock);
execAndPrint($light, $clock);
execAndPrint($light, $clock);
execAndPrint($light, $clock);
execAndPrint($light, $clock);
execAndPrint($light, $call);
execAndPrint($light, $clock);
execAndPrint($light, $clock);
execAndPrint($light, $clock);
execAndPrint($light, $clock);
execAndPrint($light, $clock);
execAndPrint($light, $clock);
execAndPrint($light, $clock);
execAndPrint($light, $clock);