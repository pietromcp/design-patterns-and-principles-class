<?php

class UserAccount {
    private string $id;
    private string $username;
    private DateTime $lastChangedAt;
    private string $avatarUrl;
    private array $preferences;

    public function getUsername(): string {
        return $this->username;
    }

    public function getAvatarUrl(): string {
        return $this->avatarUrl;
    }

    public function getPreferences(): array {
        return $this->preferences;
    }

    public function __construct($id, $username) {
        $this->id = $id;
        $this->username = $username;
        $this->preferences = array();
        $this->updateLastChangedAt();
    }

    public function changeUsername($username) {
        $this->username = $username;
        $this->lastChangedAt = new DateTime();
    }

    public function chooseAvatar($avatar) {
        $this->avatarUrl = $avatar;
        $this->updateLastChangedAt();
    }

    public function setPreference($key, $value) {
        $this->preferences[$key] = $value;
        $this->updateLastChangedAt();
    }

    private function updateLastChangedAt() {
        $this->lastChangedAt = new DateTime();
    }

    public function snapshot(): UserAccountSnapshot {
        return new UserAccountFullSnapshot($this->username, $this->avatarUrl, $this->preferences, $this->lastChangedAt, new DateTime());
    }

    public function restore(UserAccountSnapshot $snapshot) {
        $this->username = $snapshot->username;
        $this->avatarUrl = $snapshot->avatarUrl;
        $this->preferences = $snapshot->preferences;
        $this->lastChangedAt = $snapshot->lastChangedAt;
    }
}

interface UserAccountSnapshot { // Memento
    public function getDate(): DateTime;
}

class UserAccountFullSnapshot implements UserAccountSnapshot {
    // public .....
    public string $username;
    public DateTime $lastChangedAt;
    public string $avatarUrl;
    public array $preferences;
    public DateTime $createdAt;

    public function __construct($username, $avatar, $prefs, $lastChangedAt, $createdAt) {
        $this->username = $username;
        $this->avatarUrl = $avatar;
        $this->preferences = $prefs;
        $this->lastChangedAt = $lastChangedAt;
        $this->createdAt = $createdAt;
    }

    public function getDate(): DateTime {
        return $this->createdAt;
    }
}

function changeAndUpdateHistory(UserAccount $account, array &$h, $f) {
    $f($account);
    array_push($h, $account->snapshot());
}

function printAccount(UserAccount $a) {
    echo "Username: " . $a->getUsername() . "\n";
    echo "Avatar Url: " . $a->getAvatarUrl() . "\n";
    echo "Preferences: " . json_encode($a->getPreferences()) . "\n";
}

$history = array();
$account = new UserAccount("xyz", "pietrom");
changeAndUpdateHistory($account, $history, function($a) { $a->chooseAvatar("pietrom.png"); });
changeAndUpdateHistory($account, $history, function($a) { $a->chooseAvatar("martinellip.png"); });
changeAndUpdateHistory($account, $history, function($a) { $a->setPreference("x", 123); });
changeAndUpdateHistory($account, $history, function($a) { $a->setPreference("y", 456); });
changeAndUpdateHistory($account, $history, function($a) { $a->setPreference("x", 789); });
changeAndUpdateHistory($account, $history, function($a) { $a->chooseAvatar("pietrom.png"); });
changeAndUpdateHistory($account, $history, function($a) { $a->setPreference("z", 654); });
changeAndUpdateHistory($account, $history, function($a) { $a->setPreference("x", 999); });
changeAndUpdateHistory($account, $history, function($a) { $a->changeUsername("martinellip"); });

echo "=== Account after " . count($history) . " undoable changes ===\n";
printAccount($account);
$snap = $history[4];
$account->restore($snap);
echo "=== Account after restoring 5th snapshot ===\n";
printAccount($account);