package com.codiceplastico.dpdp.gof.adapter;

public interface TaxFeeProvider {
    double getFee();
}
