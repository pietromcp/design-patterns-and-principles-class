package com.codiceplastico.dpdp.gof.proxy;

import java.time.Duration;
import java.time.LocalDateTime;

public interface ConfigurationReader {
    ConfigurationData readConfiguration();
}

class ConfigurationData {
    public final int x, y;

    ConfigurationData(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "ConfigurationData{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}

class ExternalResourceAccessingConfigurationReader implements ConfigurationReader {
    @Override
    public ConfigurationData readConfiguration() {
        try {
            Thread.sleep(Duration.ofSeconds(4).toMillis());
            return new ConfigurationData(11, 19);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}

class ConfigurationReaderCachingProxy implements ConfigurationReader {
    private final ConfigurationReader reader;
    private ConfigurationData data;

    ConfigurationReaderCachingProxy(ConfigurationReader reader) {
        this.reader = reader;
    }

    @Override
    public ConfigurationData readConfiguration() {
        if(data == null) {
            data = reader.readConfiguration();
        }
        return data;
    }
}

