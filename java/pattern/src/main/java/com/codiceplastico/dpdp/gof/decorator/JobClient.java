package com.codiceplastico.dpdp.gof.decorator;

public class JobClient {
    private final Job executor;

    public JobClient(Job executor) {
        this.executor = executor;
    }

    public void doJob(String data) throws Exception {
        this.executor.execute(data);
    }

    public static void main(String[] args) throws Exception {
        var executor = new JobMetricsDecorator(
                new JobLoggingDecorator(
                        new JobExecutor()
                )
        );
        var client = new JobClient(executor);

        client.doJob("Albert");
        client.doJob("Einstein");
        client.doJob("Niels");
        client.doJob("Bohr");
        client.doJob("Stephen");
        client.doJob("Hawking");
    }
}
