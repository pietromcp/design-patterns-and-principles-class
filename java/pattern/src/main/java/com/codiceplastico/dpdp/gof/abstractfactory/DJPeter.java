package com.codiceplastico.dpdp.gof.abstractfactory;

public class DJPeter {
    public static void main(String args[]) {
        runSample("CD");
        runSample("LP");
        runSample("TAPE");
    }

    private static void runSample(String type) {
        var factory = getFactory(type);
        var musicItem = factory.createItem( "Song title");
        var otherItem = factory.createItem( "Other song");
        var thirdItem = factory.createItem( "Third song");
        var fourthItem = factory.createItem( "Fourth song");
        var musicPlayer = factory.createPlayer();
        musicPlayer.play(musicItem);
        musicPlayer.play(otherItem);
        musicPlayer.play(thirdItem);
        musicPlayer.play(fourthItem);
    }

    private static MusicFactory getFactory(String type) {
        switch (type) {
            case "CD":
                return new CdFactory();
            case "LP":
                return new LpFactory();
            case "TAPE":
                return new TapeFactory();
            default:
                throw new IllegalArgumentException(type);
        }
    }
}

interface MusicFactory {
    MusicPlayer createPlayer();

    MusicItem createItem(String title);
}

interface MusicItem {}

interface MusicPlayer {
    void play(MusicItem item);
}