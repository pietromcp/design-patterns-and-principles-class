package com.codiceplastico.dpdp.gof.bridge;

public class Service {
    private final Handler<Request> handler;

    public Service(Handler<Request> handler) {
        this.handler = handler;
    }

    void DoSomething(Request request) {
        this.handler.handle(request);
    }
}
