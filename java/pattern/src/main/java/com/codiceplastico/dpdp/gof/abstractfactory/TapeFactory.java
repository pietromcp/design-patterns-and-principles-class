package com.codiceplastico.dpdp.gof.abstractfactory;

import java.util.Random;

public class TapeFactory implements MusicFactory {
    private static final Random Rnd = new Random();
    @Override
    public MusicPlayer createPlayer() {
        return new TapePlayer();
    }

    @Override
    public MusicItem createItem(String title) {
        return new TapeItem(Rnd.nextInt(2) == 1 ? TapeSide.A : TapeSide.B, title);
    }
}

class TapePlayer implements MusicPlayer {
    @Override
    public void play(MusicItem item) {
        var song = (TapeItem)item;
        System.out.println(String.format("Playing '%s' (%s) on TapePlayer", song.getTitle(), song.getSide()));
    }
}

class TapeItem implements MusicItem {
    private final TapeSide side;
    private final String title;

    TapeItem(TapeSide side, String title) {
        this.side = side;
        this.title = title;
    }

    public TapeSide getSide() {
        return side;
    }

    public String getTitle() {
        return title;
    }
}

enum TapeSide {
    A, B
}