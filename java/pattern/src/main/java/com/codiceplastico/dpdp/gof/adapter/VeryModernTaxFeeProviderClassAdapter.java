package com.codiceplastico.dpdp.gof.adapter;

public class VeryModernTaxFeeProviderClassAdapter extends VeryModernTaxFeeProviderFromAnExternalLibrary implements TaxFeeProvider {
    @Override
    public double getFee() {
        return readTaxFee().doubleValue();
    }
}
