package com.codiceplastico.dpdp.gof.abstractfactory;

class CdFactory implements MusicFactory {
    @Override
    public MusicPlayer createPlayer() {
        return new CdPlayer();
    }

    @Override
    public MusicItem createItem(String title) {
        return new CdItem(title);
    }
}

class CdPlayer implements MusicPlayer {
    @Override
    public void play(MusicItem item) {
        System.out.println(String.format("Playing '%s' on CdPlayer", ((CdItem)item).getTitle()));
    }
}

class CdItem implements MusicItem {
    private final String title;

    CdItem(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
