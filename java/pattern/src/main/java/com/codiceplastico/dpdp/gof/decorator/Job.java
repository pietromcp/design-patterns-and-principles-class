package com.codiceplastico.dpdp.gof.decorator;

import java.time.Duration;
import java.util.Random;
import java.util.UUID;

public interface Job {
    void execute(String inputData) throws Exception;
}

class JobExecutor implements Job {
    private static final Random Random = new Random();

    @Override
    public void execute(String inputData) throws InterruptedException {
        Thread.sleep(Duration.ofSeconds(Random.nextInt(3) + 1).toMillis());
        System.out.println("JobExecutor: " + inputData);
    }
}

class JobLoggingDecorator implements Job {
    private final Job decoratee;

    JobLoggingDecorator(Job decoratee) {
        this.decoratee = decoratee;
    }

    @Override
    public void execute(String inputData) throws Exception {
        UUID uuid = UUID.randomUUID();
        System.out.println(String.format("[%s] Executing job with input '%s'", uuid.toString(), inputData));
        decoratee.execute(inputData);
        System.out.println(String.format("[%s] Job execution with input '%s' completed", uuid.toString(), inputData));
    }
}

class JobMetricsDecorator implements Job {
    private final Job decoratee;

    JobMetricsDecorator(Job decoratee) {
        this.decoratee = decoratee;
    }

    @Override
    public void execute(String inputData) throws Exception {
        var start = System.currentTimeMillis();
        decoratee.execute(inputData);
        var stop = System.currentTimeMillis();
        System.err.println(String.format("Job execution with input '%s' took %d millis", inputData, (stop - start)));
    }
}
