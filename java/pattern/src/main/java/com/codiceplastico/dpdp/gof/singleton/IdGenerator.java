package com.codiceplastico.dpdp.gof.singleton;

public class IdGenerator {
    private int lastId;

    private IdGenerator() {
        lastId = 0;
    }

    private static final IdGenerator INSTANCE = new IdGenerator();

    public static IdGenerator getInstance() {
        return INSTANCE;
    }

    public String nextId() {
        lastId++;
        return String.format("ID_%s", lastId);
    }

    public static void main(String[] args) {
        System.out.println(IdGenerator.getInstance().nextId());
        System.out.println(IdGenerator.getInstance().nextId());
        System.out.println(IdGenerator.getInstance().nextId());

        System.out.println(new Person("Albert", "Einstein"));
        System.out.println(new Person("Stephen", "Hawking"));
        System.out.println(new Person("Niels", "Bohr"));
    }
}

class Person {
    private final String id;
    private final String firstname;
    private final String lastname;

    public Person(String first, String last) {
        this.id = IdGenerator.getInstance().nextId();
        this.firstname = first;
        this.lastname = last;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id='" + id + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                '}';
    }
}
