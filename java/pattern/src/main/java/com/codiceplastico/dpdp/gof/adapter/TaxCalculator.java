package com.codiceplastico.dpdp.gof.adapter;

import java.math.BigDecimal;

public class TaxCalculator {
    public static void main(String[] args) {
        var service0 = new TaxService(
                new VeryModernTaxFeeProviderObjectAdapter(
                        new VeryModernTaxFeeProviderFromAnExternalLibrary()
                )
        );
        System.out.println(service0.calculateTax(1000));

        var service1 = new TaxService(
                new VeryModernTaxFeeProviderClassAdapter()
        );
        System.out.println(service1.calculateTax(1000));
    }
}

class VeryModernTaxFeeProviderFromAnExternalLibrary {
    public BigDecimal readTaxFee() {
        return BigDecimal.valueOf(0.31);
    }
}
