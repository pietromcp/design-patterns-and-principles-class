package com.codiceplastico.dpdp.gof.setofresponsibility;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class TaxManager {
    public static void main(String[] args) {
        var itInput = new TaxInput(1000, "IT");
        var ukInput = new TaxInput(1000, "UK");
        var deInput = new TaxInput(1000, "DE");
        var jpInput = new TaxInput(1000, "JP");

        var calculators = Arrays.asList(
                new ItTaxCalculator(),
                new DeTaxCalculator(),
                new UkTaxCalculator(),
                new FixedTaxCalculator()
        );

        var wrapper = new TaxCalculator(calculators);

        System.out.println(wrapper.calculate(itInput));
        System.out.println(wrapper.calculate(deInput));
        System.out.println(wrapper.calculate(ukInput));
        System.out.println(wrapper.calculate(jpInput));
    }
}

class TaxCalculator {
    private final Collection<SelectableTaxCalculator> calculators;

    public TaxCalculator(Collection<SelectableTaxCalculator> calculators) {
        this.calculators = calculators;
    }

    public Tax calculate(TaxInput input) {
        return calculators
                .stream()
                .filter(c -> c.canHandle(input))
                .findFirst()
                .map(c -> c.calculate(input))
                .orElseThrow();
    }
}

interface SelectableTaxCalculator {
    Tax calculate(TaxInput input);

    boolean canHandle(TaxInput input);
}

class TaxInput {
    private final double income;
    private final String countryCode;

    TaxInput(double income, String countryCode) {
        this.income = income;
        this.countryCode = countryCode;
    }

    public double getIncome() {
        return income;
    }

    public String getCountryCode() {
        return countryCode;
    }
}

class Tax {
    private final double amount;

    Tax(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Tax{" +
                "amount=" + amount +
                '}';
    }
}

class FixedTaxCalculator implements SelectableTaxCalculator {
    @Override
    public Tax calculate(TaxInput input) {
        return new Tax(219);
    }

    @Override
    public boolean canHandle(TaxInput input) {
        return true;
    }
}

class UkTaxCalculator implements SelectableTaxCalculator {
    @Override
    public Tax calculate(TaxInput input) {
        return new Tax(0.25 * input.getIncome());
    }

    @Override
    public boolean canHandle(TaxInput input) {
        return "UK".equals(input.getCountryCode());
    }
}

class DeTaxCalculator implements SelectableTaxCalculator {
    @Override
    public Tax calculate(TaxInput input) {
        return new Tax(0.31 * input.getIncome());
    }

    @Override
    public boolean canHandle(TaxInput input) {
        return "DE".equals(input.getCountryCode());
    }
}

class ItTaxCalculator implements SelectableTaxCalculator {
    @Override
    public Tax calculate(TaxInput input) {
        return new Tax(0.29 * input.getIncome());
    }

    @Override
    public boolean canHandle(TaxInput input) {
        return "IT".equals(input.getCountryCode());
    }
}