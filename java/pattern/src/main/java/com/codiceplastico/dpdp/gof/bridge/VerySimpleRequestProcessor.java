package com.codiceplastico.dpdp.gof.bridge;

public class VerySimpleRequestProcessor implements Processor<Request> {
    @Override
    public void process(Request request) {
        System.out.println(String.format("Processing request: %s %d", request.getUserId(), request.getData()));
    }
}
