package com.codiceplastico.dpdp.gof.state;

public class Disabled implements RuleState {
    @Override
    public RuleState update() {
        return this;
    }

    @Override
    public RuleState enable() {
        return new Active();
    }

    @Override
    public RuleState disable() {
        throw new UnsupportedOperationException("Already disabled rule");
    }

    @Override
    public RuleState publish() {
        throw new UnsupportedOperationException("Can't publish disabled rule");
    }
}
