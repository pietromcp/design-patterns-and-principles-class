package com.codiceplastico.dpdp.gof.state;

public class RuleManager {
    public static void main(String[] args) {
        var rule = new Rule("initial rule data");
        printRule(rule);
        doTransition(rule, (x) -> x.update("New rule data"));
        doTransition(rule, (x) -> x.enable());
        doTransition(rule, (x) -> x.disable());
        doTransition(rule, (x) -> x.publish());
        doTransition(rule, (x) -> x.publish());
        doTransition(rule, (x) -> x.update("Newest rule data"));
        doTransition(rule, (x) -> x.disable());
    }

    private static void printRule(Rule rule) {
        System.out.println(String.format("Rule state is %s", rule.toString()));
    }

    private static void doTransition(Rule rule, Action<Rule> action) {
        try {
            action.execute(rule);
        } catch(Exception e) {
            System.out.println("ERR: " + e.getMessage());
        }
        printRule(rule);
    }
}

interface Action<T> {
    void execute(T t);
}
