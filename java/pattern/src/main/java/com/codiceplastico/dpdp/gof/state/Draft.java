package com.codiceplastico.dpdp.gof.state;

public class Draft implements RuleState {
    @Override
    public RuleState update() {
        return this;
    }

    @Override
    public RuleState enable() {
        throw new UnsupportedOperationException("Can't enable draft rule");
    }

    @Override
    public RuleState disable() {
        throw new UnsupportedOperationException("Can't disable draft rule");
    }

    @Override
    public RuleState publish() {
        return new Active();
    }
}
