package com.codiceplastico.dpdp.gof.bridge;

public class Request {
    private String userId;
    private int data;

    public Request(String userId, int data) {
        this.userId = userId;
        this.data = data;
    }

    public String getUserId() {
        return userId;
    }

    public int getData() {
        return data;
    }
}
