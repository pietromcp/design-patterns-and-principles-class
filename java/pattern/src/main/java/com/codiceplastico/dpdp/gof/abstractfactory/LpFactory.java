package com.codiceplastico.dpdp.gof.abstractfactory;

public class LpFactory implements MusicFactory {
    private int tracksCount = 0;
    @Override
    public MusicPlayer createPlayer() {
        return new LpPlayer();
    }

    @Override
    public MusicItem createItem(String title) {
        tracksCount++;
        return new LpItem(tracksCount, title);
    }
}

class LpPlayer implements MusicPlayer {
    @Override
    public void play(MusicItem item) {
        LpItem song = (LpItem)item;
        System.out.println(String.format("Playing '%s' (%d) on LpPlayer", song.getSongTitle(), song.getTrackNumber()));
    }
}

class LpItem implements MusicItem {
    private final int trackNumber;
    private final String songTitle;

    LpItem(int trackNumber, String songTitle) {
        this.trackNumber = trackNumber;
        this.songTitle = songTitle;
    }

    public int getTrackNumber() {
        return trackNumber;
    }

    public String getSongTitle() {
        return songTitle;
    }
}
