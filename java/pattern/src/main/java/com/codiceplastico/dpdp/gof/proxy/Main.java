package com.codiceplastico.dpdp.gof.proxy;

import java.time.LocalDateTime;

public class Main {
    public static void main(String[] args) {
        ConfigurationReader externalReader = new ExternalResourceAccessingConfigurationReader();
        ConfigurationReader cachingReader = new ConfigurationReaderCachingProxy(externalReader);
        use(cachingReader);
    }

    private static void use(ConfigurationReader reader) {
        System.out.println(LocalDateTime.now());
        System.out.println(reader.readConfiguration());
        System.out.println(LocalDateTime.now());
        System.out.println(reader.readConfiguration());
        System.out.println(LocalDateTime.now());
        System.out.println(reader.readConfiguration());
    }
}
