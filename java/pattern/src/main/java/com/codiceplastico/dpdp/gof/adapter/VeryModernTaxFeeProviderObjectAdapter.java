package com.codiceplastico.dpdp.gof.adapter;

public class VeryModernTaxFeeProviderObjectAdapter implements TaxFeeProvider {
    private final VeryModernTaxFeeProviderFromAnExternalLibrary adaptee;

    VeryModernTaxFeeProviderObjectAdapter(VeryModernTaxFeeProviderFromAnExternalLibrary adaptee) {
        this.adaptee = adaptee;
    }

    @Override
    public double getFee() {
        return adaptee.readTaxFee().doubleValue();
    }
}
