package com.codiceplastico.dpdp.gof.adapter;

public class TaxService {
    private final TaxFeeProvider provider;

    public TaxService(TaxFeeProvider provider) {
        this.provider = provider;
    }

    public double calculateTax(double income) {
        return income * provider.getFee();
    }
}
