package com.codiceplastico.dpdp.gof.chainofresponsibility;

public class TaxManager {
    public static void main(String[] args) {
        var itInput = new TaxInput(1000, "IT");
        var ukInput = new TaxInput(1000, "UK");
        var deInput = new TaxInput(1000, "DE");
        var jpInput = new TaxInput(1000, "JP");

        var calculator = new ItTaxCalculator(
          new DeTaxCalculator(
                  new UkTaxCalculator(
                          new FixedTaxCalculator()
                  )
          )
        );

        System.out.println(calculator.calculate(itInput));
        System.out.println(calculator.calculate(deInput));
        System.out.println(calculator.calculate(ukInput));
        System.out.println(calculator.calculate(jpInput));
    }
}

interface TaxCalculator {
    Tax calculate(TaxInput input);
}

class TaxInput {
    private final double income;
    private final String countryCode;

    TaxInput(double income, String countryCode) {
        this.income = income;
        this.countryCode = countryCode;
    }

    public double getIncome() {
        return income;
    }

    public String getCountryCode() {
        return countryCode;
    }
}

class Tax {
    private final double amount;

    Tax(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Tax{" +
                "amount=" + amount +
                '}';
    }
}

class FixedTaxCalculator implements TaxCalculator {
    @Override
    public Tax calculate(TaxInput input) {
        return new Tax(219);
    }
}

class UkTaxCalculator implements TaxCalculator {
    private final TaxCalculator next;

    UkTaxCalculator(TaxCalculator next) {
        this.next = next;
    }

    @Override
    public Tax calculate(TaxInput input) {
        if("UK".equals(input.getCountryCode())) {
            return new Tax(0.25 * input.getIncome());
        }
        return next.calculate(input);
    }
}

class DeTaxCalculator implements TaxCalculator {
    private final TaxCalculator next;

    DeTaxCalculator(TaxCalculator next) {
        this.next = next;
    }

    @Override
    public Tax calculate(TaxInput input) {
        if("DE".equals(input.getCountryCode())) {
            return new Tax(0.31 * input.getIncome());
        }
        return next.calculate(input);
    }
}

class ItTaxCalculator implements TaxCalculator {
    private final TaxCalculator next;

    ItTaxCalculator(TaxCalculator next) {
        this.next = next;
    }

    @Override
    public Tax calculate(TaxInput input) {
        if("IT".equals(input.getCountryCode())) {
            return new Tax(0.29 * input.getIncome());
        }
        return next.calculate(input);
    }
}