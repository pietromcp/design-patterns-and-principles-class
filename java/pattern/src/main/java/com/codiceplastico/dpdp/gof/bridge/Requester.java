package com.codiceplastico.dpdp.gof.bridge;

public class Requester {
    public static void main(String[] args) {
        var request = new Request("pietrom", 19);
        var service = new Service(
                new ProcessorToHandlerBridge<Request>(
                        new VerySimpleRequestProcessor()
                )
        );

        service.DoSomething(request);
    }
}

