package com.codiceplastico.dpdp.gof.bridge;

public interface Handler<TReq> {
    void handle(TReq request);
}
