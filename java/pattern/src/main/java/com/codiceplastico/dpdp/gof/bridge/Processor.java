package com.codiceplastico.dpdp.gof.bridge;

public interface Processor<TReq> {
    void process(TReq request);
}
