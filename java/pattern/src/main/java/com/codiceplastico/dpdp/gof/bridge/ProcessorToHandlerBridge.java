package com.codiceplastico.dpdp.gof.bridge;

public class ProcessorToHandlerBridge<TReq> implements Handler<TReq> {
    private final Processor<TReq> processor;

    public ProcessorToHandlerBridge(Processor<TReq> processor) {
        this.processor = processor;
    }

    @Override
    public void handle(TReq request) {
        processor.process(request);
    }
}
