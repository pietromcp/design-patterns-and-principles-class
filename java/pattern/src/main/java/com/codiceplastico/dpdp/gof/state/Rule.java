package com.codiceplastico.dpdp.gof.state;

public class Rule {
    private RuleState state;

    private String data;

    public Rule(String data) {
        this.data = data;
        this.state = new Draft();
    }

    @Override
    public String toString() {
        return "Rule{" +
                "state=" + state.asString() +
                ", data='" + data + '\'' +
                '}';
    }

    public void enable() {
        state = state.enable();
    }

    public void disable() {
        state = state.disable();
    }

    public void publish() {
        state = state.publish();
    }

    public void update(String newData) {
        state = state.update();
        data = newData;
    }
}
