package com.codiceplastico.dpdp.gof.state;

public class Active implements RuleState {
    @Override
    public RuleState update() {
        throw new UnsupportedOperationException("Can't update active rule");
    }

    @Override
    public RuleState enable() {
        throw new UnsupportedOperationException("Already active rule");
    }

    @Override
    public RuleState disable() {
        return new Disabled();
    }

    @Override
    public RuleState publish() {
        throw new UnsupportedOperationException("Can't publish enabled rule");
    }
}
