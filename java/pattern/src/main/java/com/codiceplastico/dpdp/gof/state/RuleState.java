package com.codiceplastico.dpdp.gof.state;

public interface RuleState {
    RuleState update();

    RuleState enable();

    RuleState disable();

    RuleState publish();

    default String asString() {
        return getClass().getSimpleName();
    }
}
