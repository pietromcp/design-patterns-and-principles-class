package com.gildedrose;

import org.approvaltests.Approvals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;

public class GildedRoseApprovalTest {
    private PrintStream originalOut;
    private ByteArrayOutputStream out;

    @Before
    public void changeSystemOut() {
        originalOut = System.out;
        out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
    }

    @After
    public void resetSystemOut() {
        System.setOut(originalOut);
    }

    @Test
    public void exerciseSystemAndVerifyOutput() {
        TexttestFixture.main(new String[] {});
        Approvals.verify(new String(out.toByteArray(), Charset.forName("UTF-8")));
    }
}
